package com.ems.administratorapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.kafka.annotation.EnableKafka;

@SpringBootApplication
@ComponentScan("com.ems.administratorapi")
@EnableReactiveMongoRepositories("com.ems.administratorapi.repository")
@EnableAutoConfiguration(exclude={MongoAutoConfiguration.class})
@Configuration(proxyBeanMethods=false)
@ConditionalOnClass(value=org.springframework.kafka.core.KafkaTemplate.class)
@EnableConfigurationProperties(value= KafkaProperties.class)
@EnableKafka
public class AdministratorApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AdministratorApiApplication.class, args);
	}

}
