package com.ems.administratorapi.entity;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Employee {
    @Id
    private String empId;
    private String name;
    private String designation;
    private String email;
    private long contact;
    private String password;

    public Employee setEmpId(String empId) {
        this.empId = empId;
        return this;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    public Employee setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public Employee setEmail(String email) {
        this.email = email;
        return this;
    }

    public Employee setContact(long contact) {
        this.contact = contact;
        return this;
    }

    public Employee setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmpId() {
        return empId;
    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public String getEmail() {
        return email;
    }

    public long getContact() {
        return contact;
    }

    public String getPassword() {
        return password;
    }
}