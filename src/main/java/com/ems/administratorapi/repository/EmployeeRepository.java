package com.ems.administratorapi.repository;
import com.ems.administratorapi.entity.Employee;
import com.ems.administratorapi.model.api.EmployeeApiDto;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import reactor.core.publisher.Mono;

public interface EmployeeRepository extends ReactiveMongoRepository<Employee,String>{

    Mono<EmployeeApiDto> findByEmailAndPassword(String empId, String password);
}


