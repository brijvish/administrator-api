package com.ems.administratorapi.service;

import com.ems.administratorapi.entity.Employee;
import com.ems.administratorapi.model.api.EmployeeApiDto;
import com.ems.administratorapi.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@Service
public class EmployeeService {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeService.class);


    private final EmployeeRepository employeeRepository;
    private final KafkaProducerService kafkaProducer;

    @Autowired
    public EmployeeService(EmployeeRepository employeeRepository, final KafkaProducerService kafkaProducer) {
        this.employeeRepository = employeeRepository;
        this.kafkaProducer = kafkaProducer;
    }

    public Flux<Employee> findAll() {
        return employeeRepository.findAll()
                .doOnSubscribe(e -> logger.debug("Searching All Employee", e))
                .doOnComplete(() -> logger.debug("Employee retrieved"));
    }

    public Mono<Employee> create(Employee employee) {
        return Mono.fromSupplier(() -> employee)
                .flatMap(employeeRepository::insert)
                .doOnSubscribe(i -> logger.info("Employee Creating", i))
                .doOnNext(e -> logger.info("Employee Created", e));
    }

    public Mono<Employee> deleteById(String id) {
        return employeeRepository.findById(id)
                .flatMap(this::delete)
                .doOnSubscribe(i -> logger.info("Employee Deleting", i))
                .doOnNext(e -> logger.info("Employee Deleted", e));

    }

    private Mono<Employee> delete(Employee employee) {
        return Mono.justOrEmpty(employee.getEmpId())
                .flatMap(employeeRepository::deleteById)
                .doOnNext(e -> logger.info("Employee deleted by", employee.getEmpId(), e))
                .then(Mono.just(employee));

    }

    public Mono<Employee> findById(String id) {
        return employeeRepository.findById(id)
                .doOnSubscribe(i -> logger.debug("Searching Employee By id",i));
    }

    public void findByEmailAndPassword(EmployeeApiDto employeeApiDto) {
        Mono<EmployeeApiDto> employeeDto = employeeRepository.findByEmailAndPassword(employeeApiDto.getEmail(),
                employeeApiDto.getPassword());
        System.out.println("Admin ji   ===  >"+employeeDto);
        kafkaProducer.send(employeeDto.block());
                //.doOnSubscribe(i -> logger.debug("Searching Employee By id",i));
    }

    public Mono<String> getHello() {
       // System.out.println("Serives callllllllll");
        //return employeeRepository.findByEmailAndPassword("Khushi@gmail.com","kvishva");
        return Mono.just("Hello Administrator Api !");
    }
}
