package com.ems.administratorapi.service;

import com.ems.administratorapi.model.api.EmployeeApiDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
public class EmployeeDetailsService {

    private final EmployeeService employeeService;

    private static final Logger LOG = LoggerFactory.getLogger
            (String.class);

    public EmployeeDetailsService(final EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @KafkaListener(topics = "topicofemployee1", id = "ems")
    @KafkaHandler
    public void receiveEmployeeForTopic(@Payload EmployeeApiDto data,
                                   @Headers MessageHeaders headers) {
        System.out.println("topicofemployee1==== >" +data);
        employeeService.findByEmailAndPassword((EmployeeApiDto) data);
    }
}
