package com.ems.administratorapi.service;

import com.ems.administratorapi.model.api.EmployeeApiDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {
    @Autowired
    private KafkaTemplate<String, EmployeeApiDto> kafkaTemplate;
    //@Value("${statistics.kafka.topic}")
    String kafkaTopic ="topicofadmin1";
    public void send(EmployeeApiDto payload) {
        Message<EmployeeApiDto> message = MessageBuilder
                .withPayload(payload)
                .setHeader(KafkaHeaders.TOPIC, kafkaTopic)
                .build();
        System.out.println("message === >"+ message);
        kafkaTemplate.send(message);
    }

}
