package com.ems.administratorapi.mapper;

import com.ems.administratorapi.entity.Employee;
import com.ems.administratorapi.model.api.EmployeeApiDto;

public final class EmployeeMapper {

    private EmployeeMapper() {
    }

    public static EmployeeApiDto toApi(Employee input) {
        return new EmployeeApiDto()
                .setEmpId(input.getEmpId())
                .setName(input.getName())
                .setDesignation(input.getDesignation())
                .setEmail(input.getEmail())
                .setContact(input.getContact())
                .setPassword(input.getPassword());
    }

    public static Employee toEntity(EmployeeApiDto input) {
        return new Employee()
                .setEmpId(input.getEmpId())
                .setName(input.getName())
                .setDesignation(input.getDesignation())
                .setEmail(input.getEmail())
                .setContact(input.getContact())
                .setPassword(input.getPassword());
    }

    /*private static Duration duration(List<Question> questions) {
        return Duration.ofSeconds(questions.stream().mapToLong(Question::getDurationInSeconds).sum());
    }*/
}
