package com.ems.administratorapi.model.api;


import org.apache.kafka.common.serialization.Deserializer;

public class EmployeeApiDtoDeserializer implements Deserializer {
    private String empId;
    private String name;
    private String designation;
    private String email;
    private long contact;
    private String password;

    public EmployeeApiDtoDeserializer setEmpId(String empId) {
        this.empId = empId;
        return this;
    }

    public EmployeeApiDtoDeserializer setName(String name) {
        this.name = name;
        return this;
    }

    public EmployeeApiDtoDeserializer setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public EmployeeApiDtoDeserializer setEmail(String email) {
        this.email = email;
        return this;
    }

    public EmployeeApiDtoDeserializer setContact(long contact) {
        this.contact = contact;
        return this;
    }

    public EmployeeApiDtoDeserializer setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmpId() {
        return empId;
    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public String getEmail() {
        return email;
    }

    public long getContact() {
        return contact;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public Object deserialize(String topic, byte[] data) {
        return this;
    }
}