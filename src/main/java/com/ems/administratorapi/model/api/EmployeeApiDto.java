package com.ems.administratorapi.model.api;


import org.apache.kafka.common.serialization.Serializer;

public class EmployeeApiDto implements Serializer {
    private String empId;
    private String name;
    private String designation;
    private String email;
    private long contact;
    private String password;

    public EmployeeApiDto setEmpId(String empId) {
        this.empId = empId;
        return this;
    }

    public EmployeeApiDto setName(String name) {
        this.name = name;
        return this;
    }

    public EmployeeApiDto setDesignation(String designation) {
        this.designation = designation;
        return this;
    }

    public EmployeeApiDto setEmail(String email) {
        this.email = email;
        return this;
    }

    public EmployeeApiDto setContact(long contact) {
        this.contact = contact;
        return this;
    }

    public EmployeeApiDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getEmpId() {
        return empId;
    }

    public String getName() {
        return name;
    }

    public String getDesignation() {
        return designation;
    }

    public String getEmail() {
        return email;
    }

    public long getContact() {
        return contact;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public byte[] serialize(String topic, Object data) {
        return new byte[0];
    }
}