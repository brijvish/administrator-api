package com.ems.administratorapi.resource;

import com.ems.administratorapi.entity.Employee;
import com.ems.administratorapi.mapper.EmployeeMapper;
import com.ems.administratorapi.model.api.EmployeeApiDto;
import com.ems.administratorapi.service.EmployeeService;
import com.ems.administratorapi.service.KafkaProducerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@RequestMapping("/api/employee")
class EmployeeResource {
    private static final Logger logger = LoggerFactory.getLogger(EmployeeResource.class);
    private final EmployeeService employeeService;
    private final KafkaProducerService kafkaProducer;

    public EmployeeResource(EmployeeService employeeService, final KafkaProducerService kafkaProducer) {
        this.employeeService = employeeService;
        this.kafkaProducer = kafkaProducer;
    }

    @GetMapping
    @RequestMapping("/findall")
    public Flux<EmployeeApiDto> findAll() {
        return employeeService.findAll()
                .map(EmployeeMapper::toApi);
    }

    @GetMapping
    @RequestMapping("/findbyid")
    public Mono<EmployeeApiDto> findById(@RequestParam String empId) {
        return employeeService.findById(empId)
                .map(EmployeeMapper::toApi);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @RequestMapping("/create")
    public Mono<EmployeeApiDto> create(@RequestBody EmployeeApiDto Employee) {
        Mono<EmployeeApiDto> createdEmployeeMono = employeeService.create(EmployeeMapper.toEntity(Employee))
                .map(EmployeeMapper::toApi);
        return createdEmployeeMono;
    }

    @DeleteMapping
    @RequestMapping("/delete")
    public Mono<Employee> deleteById(@RequestParam String empId) {
        return employeeService.deleteById(empId);
    }

    @GetMapping
    @RequestMapping("/hello")
    public Mono<String> hello(@RequestParam String empId) {
        return Mono.just("Hello Guys");
    }

}