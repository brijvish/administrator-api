package com.ems.administratorapi.resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin")
class HelloResource {

    @GetMapping
    @RequestMapping("/test")
    @ResponseStatus(HttpStatus.ACCEPTED)
    @ResponseBody
    public String test() {
        return "Hello Administrator Api !";
    }
}