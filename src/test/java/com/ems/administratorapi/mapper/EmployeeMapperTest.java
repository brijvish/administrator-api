package com.ems.administratorapi.mapper;

import com.ems.administratorapi.entity.Employee;
import com.ems.administratorapi.model.api.EmployeeApiDto;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.ems.administratorapi.mapper.EmployeeMapper.toApi;
import static com.ems.administratorapi.mapper.EmployeeMapper.toEntity;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class EmployeeMapperTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void toApi_shouldMap() {
        Employee input = new Employee().setEmpId("52448").setName("Smith");
        EmployeeApiDto expected = toApi(input);
        assertThat(expected.getEmpId()).isEqualTo(input.getEmpId());
        assertThat(expected.getName()).isEqualTo(input.getName());
    }

    @Test
    void toEntity_shouldMap() {
        EmployeeApiDto input = new EmployeeApiDto().setEmpId("52448").setName("Smith");
        Employee expected = toEntity(input);
        assertThat(expected.getEmpId()).isEqualTo(input.getEmpId());
        assertThat(expected.getName()).isEqualTo(input.getName());
    }
}