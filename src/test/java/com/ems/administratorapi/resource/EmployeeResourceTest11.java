//package com.ems.administratorapi.resource;
//import com.ems.administratorapi.entity.Employee;
//import com.ems.administratorapi.service.EmployeeService;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.web.reactive.server.WebTestClient;
//import reactor.core.publisher.Mono;
//
//
////@WebFluxTest(EmployeeResource.class)
//@SpringBootTest
//class EmployeeResourceTest11 {
//
//    //@Autowired
//    WebTestClient webTestClient;
//
//    @MockBean
//    private EmployeeService employeeService;
//
//   /* @Test
//    public void testGetEmployeeById() {
//        Employee employee = new Employee()
//                .setEmpId("BV20085")
//                .setName("Ravi")
//                .setDesignation("SSE")
//                .setEmail("ravi@gmail.com")
//                .setContact(8855445222L)
//                .setPassword("Ravi@123");
//        Mono<Employee> employeeMono = Mono.just(employee);
//
//        //when(employeeService.getEmployeeById(1)).thenReturn(employeeMono);
//
//        webTestClient.get()
//                .uri("/api/employee")
//                .accept(MediaType.APPLICATION_JSON)
//                .exchange()
//                .expectStatus().isOk()
//                .expectBody(Employee.class)
//                .value(employee1 -> employee.getEmpId(), equalTo("BV20085"));
//    }
//
//    @Test
//    public void testDeleteEmployeeById() {
//
//        when(employeeService.deleteEmployeeById(1)).thenReturn(Mono.just("Employee with id 1 is deleted."));
//
//        webTestClient.delete()
//                .uri("/employees/1")
//                .exchange()
//                .expectStatus().isOk()
//                .expectBody(String.class)
//                .isEqualTo("Employee with id 1 is deleted.");
//
//    }*/
//
//    @Test
//    public void testCreateEmployee() {
//        webTestClient = WebTestClient.bindToServer()
//                .baseUrl("http://localhost:" + "8000")
//                .build();
//        Employee employee = new Employee()
//                .setEmpId("BV20085")
//                .setName("Ravi")
//                .setDesignation("SSE")
//                .setEmail("ravi@gmail.com")
//                .setContact(8855445222L)
//                .setPassword("Ravi@123");
//        Mono<Employee> employeeMono = Mono.just(employee);
//        //when(employeeService.create(employee)).thenReturn(employeeMono);
//
//        webTestClient.post()
//                .uri("/api/employee/create")
//                .body(Mono.just(employee), Employee.class)
//                .exchange()
//                .expectStatus().isCreated();
//
//    }
//}