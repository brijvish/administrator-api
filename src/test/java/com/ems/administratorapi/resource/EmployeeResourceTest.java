//package com.ems.administratorapi.resource;
//
//import com.ems.administratorapi.entity.Employee;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.web.client.TestRestTemplate;
//import org.springframework.boot.web.server.WebServer;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.event.annotation.AfterTestClass;
//import org.springframework.test.context.event.annotation.BeforeTestClass;
//import org.springframework.test.web.reactive.server.FluxExchangeResult;
//import org.springframework.test.web.reactive.server.WebTestClient;
//import reactor.core.publisher.Flux;
//import reactor.core.publisher.Mono;
//import reactor.test.StepVerifier;
//
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@AutoConfigureWebTestClient(timeout = "10000")
//class EmployeeResourceTest {
//
//    @Mock
//    private EmployeeResource employeeResource;
//    //@Autowired
//    //private WebTestClient webTestClient;
//    Employee employee;
//    Mono<Employee> employeeMono;
//    Flux<Employee> employeeFlux;
//    //@LocalServerPort
//    //private int port = 8000;
//
//    @Autowired
//    private static WebTestClient client;
//    private static WebServer server;
//
//    @BeforeTestClass
//    public static void setup() throws Exception {
//        //server = new AdministratorApiApplication().start();
//        client = WebTestClient.bindToServer()
//                .baseUrl("http://localhost:" + "8000")
//                .build();
//    }
//
//    @AfterTestClass
//    public static void destroy() {
//       // server.stop();
//    }
//
//    @Mock
//    private TestRestTemplate restTemplate;
//
//    EmployeeResourceTest() {
//    }
//
//    @BeforeEach
//    void setUp() {
//        client = WebTestClient.bindToServer()
//                .baseUrl("http://localhost:" + "8000")
//                .build();
//        employee = new Employee()
//                .setEmpId("BV20085")
//                .setName("Ravi")
//                .setDesignation("SSE")
//                .setEmail("ravi@gmail.com")
//                .setContact(8855445222L)
//                .setPassword("Ravi@123");
//        employeeMono = Mono.just(employee);
//        employeeFlux = Flux.just(employee);
//
//        /*webTestClient = webTestClient
//                .mutate()
//                .responseTimeout(Duration.ofMillis(36000))
//                .build();*/
//    }
//
//    @AfterEach
//    void tearDown() {
//    }
//
//    @Test
//    void findAll() {
//    }
//
//    @Test
//    void findById() {
//    }
//
//    @Test
//    void create(){
//        //HttpEntity<Employee> entity = new HttpEntity<Employee>(product,headers);
//        FluxExchangeResult<String> result = client.post()
//                .uri("/api/employee/create")
//                .bodyValue(employee) //
//
//                .exchange()
//                //.exchange("http://localhost:8080//api/employee/create",
//                  //      HttpMethod.POST, HttpEntity.EMPTY, String.class) //
//                .expectStatus().isCreated() //
//                .expectHeader().contentTypeCompatibleWith(MediaType.TEXT_EVENT_STREAM) //
//                .returnResult(String.class);
//
//        StepVerifier.create(result.getResponseBody()) //
//                .expectNext("hello") //
//                .thenCancel() //
//                .verify();
//
//
//
//        /*Mockito.when(employeeResource.create(employee)).thenReturn(employeeMono);
//        StepVerifier.create(employeeService.create(employee))
//                .expectNext(employeeMono.block())
//                .verifyComplete();*/
//
//       /* ResponseEntity<String> response = restTemplate.getForEntity(
//                new URL("http://localhost:" + port + "/api/employee/hello").toString(), String.class);
//        assertEquals("Hello Guys", response.getBody());
//        StepVerifier.create(employeeResource.create(employee))
//                .expectNext(employeeMono.block())
//                .verifyComplete();
//
//        assertEquals("Hello Controller", response.getBody());*/
//
//        //StepVerifier.create(
//            /*    WebClient
//                .create("http://localhost:8080")
//                .post()
//                .uri(URI.create("/api/employee/hello"))
//                .body(BodyInserters.fromObject("data"));*/
//    //);
//
//    }
//
//    @Test
//    void deleteById() {
//    }
//}