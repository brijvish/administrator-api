package com.ems.administratorapi.service;

import com.ems.administratorapi.entity.Employee;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@SpringBootTest
class EmployeeServiceTest {

    @Mock
    private EmployeeService employeeService;

    Employee employee;
    Mono<Employee> employeeMono;
    Flux<Employee> employeeFlux;

    @BeforeEach
    void setUp(){
        employee = new Employee()
                .setEmpId("BV20085")
                .setName("Ravi")
                .setDesignation("SSE")
                .setEmail("ravi@gmail.com")
                .setContact(8855445222L)
                .setPassword("Ravi@123");
        employeeMono = Mono.just(employee);
        employeeFlux = Flux.just(employee);
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void findAll() {
        Mockito.when(employeeService.findAll()).thenReturn(employeeFlux);
        StepVerifier.create(employeeService.findAll())
                .expectNext(employeeFlux.blockFirst())
                .verifyComplete();
    }

    @Test
    void create() {

        Mockito.when(employeeService.create(employee)).thenReturn(employeeMono);
        StepVerifier.create(employeeService.create(employee))
                .expectNext(employeeMono.block())
                .verifyComplete();
    }

    @Test
    void deleteById() {
        Mockito.when(employeeService.deleteById(employee.getEmpId())).thenReturn(employeeMono);
        StepVerifier.create(employeeService.deleteById(employee.getEmpId()))
                .expectNext(employeeMono.block())
                .verifyComplete();
    }

    @Test
    void findById() {
        Mockito.when(employeeService.findById(employee.getEmpId())).thenReturn(employeeMono);
        StepVerifier.create(employeeService.findById(employee.getEmpId()))
                .expectNext(employeeMono.block())
                .verifyComplete();
    }

    /*@Test
    void findByUserAndPassword() {
        Mockito.when(employeeService.findByEmailAndPassword(employee.getEmail(),employee.getPassword())).thenReturn(Mono.just("Hello Administrator Api !"));
        StepVerifier.create(employeeService.getHello())
                .expectNext("Hello Administrator Api !")
                .verifyComplete();
    }*/

    @Test
    void getHello() {
        Mockito.when(employeeService.getHello()).thenReturn(Mono.just("Hello Administrator Api !"));
        StepVerifier.create(employeeService.getHello())
                .expectNext("Hello Administrator Api !")
                .verifyComplete();
    }
}