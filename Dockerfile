FROM openjdk:11
ADD build/libs/administrator-api-0.0.1-SNAPSHOT.jar  administrator-api-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "administrator-api-0.0.1-SNAPSHOT.jar"]